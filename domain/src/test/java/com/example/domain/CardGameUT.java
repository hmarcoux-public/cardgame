package com.example.domain;

import static com.example.domain.CardSuit.CLUBS;
import static com.example.domain.CardSuit.DIAMONDS;
import static com.example.domain.CardSuit.HEARTS;
import static com.example.domain.CardSuit.SPADES;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public final class CardGameUT {

    private static final Integer ANY_GAME_ID = 1;

    private static final int ANY_DECK_ID = 1;

    private static final String PLAYER_NAME = "Steeve";

    private static final String FIRST_PLAYER = "FirstPlayer";

    private static final String SECOND_PLAYER = "SecondPlayer";

    private static final String THIRD_PLAYER = "ThirdPlayer";

    private static final String UNEXISTING_PLAYER_NAME = "Unexisting";

    private CardGame game;

    public CardGameUT() {

        super();
    }

    @Before
    public void initialize() {

        this.game = new CardGame(ANY_GAME_ID);
    }

    @Test
    public void whenAddingDeck_thenDeckIsAdded() {

        final Deck deck = new CardDeck(ANY_DECK_ID);

        this.game.addDeck(deck);

        assertThat(this.game.containsDeck(ANY_DECK_ID)).isTrue();
    }

    @Test
    public void whenAddingPlayer_thenPlayerIsAdded() {

        this.game.addPlayer(PLAYER_NAME);

        assertThat(this.game.containsPlayer(PLAYER_NAME)).isTrue();
    }

    @Test
    public void whenAddingMultiplePlayers_thenAllPlayersAreAdded() {

        this.game.addPlayer(FIRST_PLAYER);
        this.game.addPlayer(SECOND_PLAYER);
        this.game.addPlayer(THIRD_PLAYER);

        assertThat(this.game.getPlayers()).extracting(Player::getName).containsOnly(FIRST_PLAYER, SECOND_PLAYER,
            THIRD_PLAYER);
    }

    @Test
    public void givenExistingPlayerName_whenAddingSamePlayer_thenNothingIsAdded() {

        this.game.addPlayer(PLAYER_NAME);
        this.game.addPlayer(PLAYER_NAME);

        assertThat(this.game.getPlayers()).extracting(Player::getName).containsOnly(PLAYER_NAME);
    }

    @Test
    public void givenExistingPlayer_whenRemovingPlayer_thenPlayerIsRemoved() {

        givenExistingPlayer(PLAYER_NAME);

        this.game.removePlayer(PLAYER_NAME);

        assertThat(this.game.getPlayers()).isEmpty();
    }

    @Test
    public void givenUnexistingPlayer_whenRemovingIt_thenNothingIsRemoved() {

        givenExistingPlayer(PLAYER_NAME);

        this.game.removePlayer(UNEXISTING_PLAYER_NAME);

        assertThat(this.game.getPlayers()).extracting(Player::getName).containsOnly(PLAYER_NAME);
    }

    @Test
    public void givenExistingPlayer_whenDealingCardsToHim_thenPlayerHasCards() {

        givenExistingPlayer(PLAYER_NAME);
        givenGameHasDeck();

        this.game.dealCardsToPlayer(PLAYER_NAME, 3);

        assertThat(this.game.getPlayer(PLAYER_NAME).getCards()).hasSize(3);
        assertThat(this.game.remainingCardsCountInShoe()).isEqualTo(49);
    }

    @Test
    public void givenUnexistingPlayer_whenDealingCardsToHim_thenNoCardsAreDealt() {

        givenGameHasDeck();

        this.game.dealCardsToPlayer(UNEXISTING_PLAYER_NAME, 3);

        assertThat(this.game.remainingCardsCountInShoe()).isEqualTo(52);
    }

    @Test
    public void givenMultiplePlayers_whenDealingCardsToAllOfThem_thenRemainingCardsCountInShoeIsDecremented() {

        givenExistingPlayer(FIRST_PLAYER);
        givenExistingPlayer(SECOND_PLAYER);
        givenExistingPlayer(THIRD_PLAYER);
        givenGameHasDeck();

        this.game.dealCardsToPlayer(FIRST_PLAYER, 5);
        this.game.dealCardsToPlayer(SECOND_PLAYER, 8);
        this.game.dealCardsToPlayer(THIRD_PLAYER, 1);

        assertThat(this.game.remainingCardsCountInShoe()).isEqualTo(38);
    }

    @Test
    public void givenExistingPlayer_whenGettingHisCards_thenPlayerCardsAreReturned() {

        givenExistingPlayer(PLAYER_NAME);
        givenPlayerHasCards(PLAYER_NAME, SPADES.card(4), SPADES.card(12));

        final List<Card> cards = this.game.getPlayerCards(PLAYER_NAME);

        assertThat(cards).containsOnly(SPADES.card(4), SPADES.card(12));
    }

    @Test
    public void givenExistingPlayerWithNoCards_whenGettingHisCards_thenNoCardsAreReturned() {

        givenExistingPlayer(PLAYER_NAME);

        final List<Card> cards = this.game.getPlayerCards(PLAYER_NAME);

        assertThat(cards).isEmpty();
    }

    @Test
    public void givenUnexistingPlayer_whenGettingHisCards_thenNoCardsAreReturned() {

        final List<Card> cards = this.game.getPlayerCards(UNEXISTING_PLAYER_NAME);

        assertThat(cards).isEmpty();
    }

    @Test
    public void givenExistingDeck_whenGettingRemainingCardsInShoe_thenCardsAreReturned() {

        givenGameHasDeck();

        final List<Card> cards = this.game.allRemainingCardsInShoe();

        assertThat(cards).hasSize(52);
    }

    @Test
    public void givenExistingDeck_whenShufflingShoe_thenShoeIsShuffled() {

        givenGameHasDeck();

        this.game.shuffleShoe();
        final List<Card> cards = this.game.allRemainingCardsInShoe();

        assertThat(cards).doesNotContainSequence(HEARTS.card(1), HEARTS.card(2), HEARTS.card(3), HEARTS.card(4));
        assertThat(cards).doesNotContainSequence(SPADES.card(1), SPADES.card(2), SPADES.card(3), SPADES.card(4));
        assertThat(cards).doesNotContainSequence(DIAMONDS.card(1), DIAMONDS.card(2), DIAMONDS.card(3),
            DIAMONDS.card(4));
        assertThat(cards).doesNotContainSequence(CLUBS.card(1), CLUBS.card(2), CLUBS.card(3), CLUBS.card(4));
    }

    private void givenGameHasDeck() {

        this.game.addDeck(new CardDeck(ANY_DECK_ID));
    }

    private void givenExistingPlayer(
        final String playerName) {

        this.game.addPlayer(playerName);
    }

    private void givenPlayerHasCards(
        final String playerName,
        final Card... cards) {

        final Player player = this.game.getPlayer(playerName);
        player.addCards(Arrays.stream(cards).collect(toList()));
    }

}
