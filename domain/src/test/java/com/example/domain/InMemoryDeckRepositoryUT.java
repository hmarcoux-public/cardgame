package com.example.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import org.junit.Before;
import org.junit.Test;

import com.example.domain.exceptions.DeckNotFoundException;

public final class InMemoryDeckRepositoryUT {

    private static final int FIRST_DECK_ID = 1;

    private static final int NEXT_DECK = 2;

    private static final int UNEXISTING_DECK_ID = 100;

    private InMemoryDeckRepository repository;

    public InMemoryDeckRepositoryUT() {

        super();
    }

    @Before
    public void initialize() {

        this.repository = new InMemoryDeckRepository();
    }

    @Test
    public void givenNoExistingDecks_whenCreatingDeck_thenDeckIsSuccessfullyCreated() {

        final Deck deck = this.repository.createDeck();

        assertThat(deck).isNotNull();
        assertThat(deck.getId()).isEqualTo(FIRST_DECK_ID);
    }

    @Test
    public void givenAnExistingDeck_whenAnotherDeckIsCreated_thenNewDeckHasNextIncrementalId() {

        givenAnExistingDeck();

        final Deck deck = this.repository.createDeck();

        assertThat(deck).isNotNull();
        assertThat(deck.getId()).isEqualTo(NEXT_DECK);
    }

    @Test
    public void givenAnExistingDeck_whenFetchingItById_thenDeckIsSuccessfullyRetrieved() {

        givenAnExistingDeck();

        final Deck deck = this.repository.fetchById(FIRST_DECK_ID);

        assertThat(deck).isNotNull();
        assertThat(deck.getId()).isEqualTo(FIRST_DECK_ID);
    }

    @Test
    public void givenExistingDecks_whenFetchingUnexistingDeckId_thenDeckNotFoundExceptionIsThrown() {

        givenAnExistingDeck();
        givenAnExistingDeck();
        givenAnExistingDeck();

        final Throwable throwable = catchThrowable(() -> this.repository.fetchById(UNEXISTING_DECK_ID));

        assertThat(throwable).isInstanceOf(DeckNotFoundException.class);
    }

    @Test
    public void givenExistingDecks_whenClearingThem_thenNoDecksAreRemaining() {

        givenAnExistingDeck();
        givenAnExistingDeck();

        this.repository.clearAll();

        assertThat(catchThrowable(() -> this.repository.fetchById(1))).isInstanceOf(DeckNotFoundException.class);
        assertThat(catchThrowable(() -> this.repository.fetchById(2))).isInstanceOf(DeckNotFoundException.class);
    }

    @Test
    public void givenExistingDecks_whenClearingThem_thenNextCreatedDeckStartsBackWithFirstId() {

        givenAnExistingDeck();
        givenAnExistingDeck();

        this.repository.clearAll();
        final Deck deck = this.repository.createDeck();

        assertThat(deck.getId()).isEqualTo(FIRST_DECK_ID);
    }

    private void givenAnExistingDeck() {

        this.repository.createDeck();
    }
}
