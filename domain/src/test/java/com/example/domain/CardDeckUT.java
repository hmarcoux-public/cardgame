package com.example.domain;

import static com.example.domain.CardSuit.CLUBS;
import static com.example.domain.CardSuit.DIAMONDS;
import static com.example.domain.CardSuit.HEARTS;
import static com.example.domain.CardSuit.SPADES;
import static java.lang.Boolean.FALSE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public final class CardDeckUT {

    private static final Integer ANY_DECK_ID = 1;

    private static final int ALL_CARDS = 52;

    private static final int MORE_CARDS_THAN_TOTAL_IN_DECK = 60;

    private Deck deck;

    public CardDeckUT() {

        super();
    }

    @Before
    public void initialize() {

        this.deck = new CardDeck(ANY_DECK_ID);
    }

    @Test
    public void givenFullDeck_whenGettingRemainingCards_thenFullDeckIsRemaining() {

        assertThat(this.deck.remainingCards()).hasSize(52);
        assertThat(this.deck.hasRemainingCards()).isTrue();
    }

    @Test
    public void givenFullDeck_whenDealingCards_thenCardsAreRemovedFromTheDeck() {

        final List<Card> cards = this.deck.dealCards(5);

        assertThat(this.deck.remainingCards()).hasSize(47);
        assertThat(this.deck.hasRemainingCards()).isTrue();
        assertThat(cards).hasSize(5);
    }

    @Test
    public void givenFullDeck_whenDealingAllCards_thenNoCardsAreRemaining() {

        final List<Card> cards = this.deck.dealCards(ALL_CARDS);

        assertThat(this.deck.hasRemainingCards()).isEqualTo(FALSE);
        assertThat(cards).hasSize(ALL_CARDS);
    }

    @Test
    public void givenFullDeck_whenDealingMoreCardsThanTotalRemainingInTheDeck_thenOnlyRemainingCardsAreDealt() {

        final List<Card> cards = this.deck.dealCards(MORE_CARDS_THAN_TOTAL_IN_DECK);

        assertThat(this.deck.hasRemainingCards()).isEqualTo(FALSE);
        assertThat(cards).hasSize(ALL_CARDS);
    }

    @Test
    public void givenFullDeck_whenDealingZeroCards_thenNoCardsAreDealt() {

        final List<Card> cards = this.deck.dealCards(0);

        assertThat(this.deck.remainingCards()).hasSize(ALL_CARDS);
        assertThat(cards).hasSize(0);
    }

    @Test
    public void givenFullDeckUnshuffled_whenDealingCards_thenRemovedCardsAreThoseOnTopOfTheDeck() {

        final List<Card> cards = this.deck.dealCards(3);

        assertThat(cards).extracting(Card::getSuit, Card::getFaceValue).containsExactly(tuple(HEARTS, 1),
            tuple(HEARTS, 2), tuple(HEARTS, 3));
    }

    @Test
    public void givenFullDeck_whenShuffling_thenCardsAreShuffled() {

        this.deck.shuffle();

        final List<Card> cards = this.deck.remainingCards();

        assertThat(cards).doesNotContainSequence(HEARTS.card(1), HEARTS.card(2), HEARTS.card(3), HEARTS.card(4));
        assertThat(cards).doesNotContainSequence(SPADES.card(1), SPADES.card(2), SPADES.card(3), SPADES.card(4));
        assertThat(cards).doesNotContainSequence(DIAMONDS.card(1), DIAMONDS.card(2), DIAMONDS.card(3),
            DIAMONDS.card(4));
        assertThat(cards).doesNotContainSequence(CLUBS.card(1), CLUBS.card(2), CLUBS.card(3), CLUBS.card(4));
    }
}
