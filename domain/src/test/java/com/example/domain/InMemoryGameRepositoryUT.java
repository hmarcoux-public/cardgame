package com.example.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import org.junit.Before;
import org.junit.Test;

import com.example.domain.exceptions.GameNotFoundException;

public final class InMemoryGameRepositoryUT {

    private static final int FIRST_GAME_ID = 1;

    private static final int NEXT_GAME_ID = 2;

    private static final int UNEXISTING_GAME_ID = 100;

    private InMemoryGameRepository repository;

    public InMemoryGameRepositoryUT() {

        super();
    }

    @Before
    public void initialize() {

        this.repository = new InMemoryGameRepository();
    }

    @Test
    public void givenNoExistingGames_whenCreatingGame_thenGameIsSuccessfullyCreated() {

        final Game game = this.repository.createGame();

        assertThat(game).isNotNull();
        assertThat(game.getId()).isEqualTo(FIRST_GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenAnotherGameIsCreated_thenNewGameHasNextIncrementalId() {

        givenAnExistingGame();

        final Game game = this.repository.createGame();

        assertThat(game).isNotNull();
        assertThat(game.getId()).isEqualTo(NEXT_GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenFetchingItById_thenGameIsSuccessfullyRetrieved() {

        givenAnExistingGame();

        final Game game = this.repository.fetchById(FIRST_GAME_ID);

        assertThat(game).isNotNull();
        assertThat(game.getId()).isEqualTo(FIRST_GAME_ID);
    }

    @Test
    public void givenExistingGames_whenFetchingUnexistingGameId_thenExceptionIsThrown() {

        givenAnExistingGame();
        givenAnExistingGame();
        givenAnExistingGame();

        final Throwable throwable = catchThrowable(() -> this.repository.fetchById(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenDeletingIt_thenGameIsSuccessfullyDeleted() {

        givenAnExistingGame();

        this.repository.delete(FIRST_GAME_ID);
        final Throwable throwable = catchThrowable(() -> this.repository.fetchById(FIRST_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenUnexistingGame_whenDeletingIt_thenExceptionIsThrown() {

        givenAnExistingGame();

        final Throwable throwable = catchThrowable(() -> this.repository.delete(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGames_whenClearingThem_thenNoGamesAreRemaining() {

        givenAnExistingGame();
        givenAnExistingGame();

        this.repository.clearAll();

        assertThat(catchThrowable(() -> this.repository.fetchById(1))).isInstanceOf(GameNotFoundException.class);
        assertThat(catchThrowable(() -> this.repository.fetchById(2))).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGames_whenClearingThem_thenNextCreatedGameStartsBackWithFirstId() {

        givenAnExistingGame();
        givenAnExistingGame();

        this.repository.clearAll();
        final Game game = this.repository.createGame();

        assertThat(game.getId()).isEqualTo(FIRST_GAME_ID);
    }

    private void givenAnExistingGame() {

        this.repository.createGame();
    }
}
