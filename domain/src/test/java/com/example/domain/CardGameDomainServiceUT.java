package com.example.domain;

import static com.example.domain.CardSuit.CLUBS;
import static com.example.domain.CardSuit.DIAMONDS;
import static com.example.domain.CardSuit.HEARTS;
import static com.example.domain.CardSuit.SPADES;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.entry;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.domain.exceptions.DeckNotFoundException;
import com.example.domain.exceptions.GameNotFoundException;
import com.example.domain.exceptions.PlayerNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public final class CardGameDomainServiceUT {

    private static final int GAME_ID = 1;

    private static final int DECK_ID = 5;

    private static final GameNotFoundException GAME_NOT_FOUND_EXCEPTION = new GameNotFoundException(GAME_ID);

    private static final DeckNotFoundException DECK_NOT_FOUND_EXCEPTION = new DeckNotFoundException(DECK_ID);

    private static final String PLAYER_NAME = "Steeve";

    private static final int DEALT_CARDS_COUNT = 4;

    private static final List<Card> ANY_CARDS = Lists.list(HEARTS.card(5), DIAMONDS.card(10));

    private static final Player PLAYER_WITH_MIDDLE_TOTAL = createPlayerWithCardValues("PlayerMiddle", 4, 8, 11);

    private static final Player PLAYER_WITH_LOWEST_TOTAL = createPlayerWithCardValues("PlayerLowest", 5, 1, 2);

    private static final Player PLAYER_WITH_HIGHEST_TOTAL = createPlayerWithCardValues("PlayerHighest", 10, 12, 13);

    @Mock
    private Game game;

    @Mock
    private Deck deck;

    @Mock
    private DeckRepository deckRepository;

    @Mock
    private GameRepository gameRepository;

    @InjectMocks
    private CardGameDomainService service;

    public CardGameDomainServiceUT() {

        super();
    }

    @Test
    public void givenExistingGameAndDeck_whenAddingDeckToGame_thenDeckIsSuccessfullyAdded() {

        givenExistingGame(GAME_ID);
        givenExistingDeck(DECK_ID);

        this.service.addDeckToGame(GAME_ID, DECK_ID);

        verify(this.game).addDeck(this.deck);
    }

    @Test
    public void givenUnexistingGame_whenAddingDeckToGame_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);
        givenExistingDeck(DECK_ID);

        final Throwable throwable = catchThrowable(() -> this.service.addDeckToGame(GAME_ID, DECK_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingDeck_whenAddingDeckToGame_thenDeckNotFoundExceptionIsThrown() {

        givenExistingGame(GAME_ID);
        givenUnexistingDeck(DECK_ID);

        final Throwable throwable = catchThrowable(() -> this.service.addDeckToGame(GAME_ID, DECK_ID));

        assertThat(throwable).isInstanceOf(DeckNotFoundException.class);
    }

    @Test
    public void givenExistingGame_whenAddingPlayerToGame_thenPlayerIsSuccessfullyAdded() {

        givenExistingGame(GAME_ID);

        this.service.addPlayerToGame(GAME_ID, PLAYER_NAME);

        verify(this.game).addPlayer(PLAYER_NAME);
    }

    @Test
    public void givenUnexistingGame_whenAddingPlayerToGame_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.addPlayerToGame(GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameAndPlayer_whenRemovingPlayerFromGame_thenPlayerIsSuccessfullyRemoved() {

        givenExistingGame(GAME_ID);
        givenExistingPlayer(PLAYER_NAME);

        this.service.removePlayerFromGame(GAME_ID, PLAYER_NAME);

        verify(this.game).removePlayer(PLAYER_NAME);
    }

    @Test
    public void givenUnexistingGame_whenRemovingPlayerFromGame_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.removePlayerFromGame(GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingPlayer_whenRemovingPlayerFromGame_thenPlayerNotFoundExceptionIsThrown() {

        givenExistingGame(GAME_ID);
        givenUnexistingPlayer(PLAYER_NAME);

        final Throwable throwable = catchThrowable(() -> this.service.removePlayerFromGame(GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(PlayerNotFoundException.class);
    }

    @Test
    public void givenExistingGameAndExistingPlayer_whenDealingCardsToPlayer_thenPlayerHasCards() {

        givenExistingGame(GAME_ID);
        givenExistingPlayer(PLAYER_NAME);

        this.service.dealCardsToPlayer(GAME_ID, PLAYER_NAME, DEALT_CARDS_COUNT);

        verify(this.game).dealCardsToPlayer(PLAYER_NAME, DEALT_CARDS_COUNT);
    }

    @Test
    public void givenUnexistingGame_whenDealingCardsToPlayer_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable =
            catchThrowable(() -> this.service.dealCardsToPlayer(GAME_ID, PLAYER_NAME, DEALT_CARDS_COUNT));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingPlayer_whenDealingCardsToPlayer_thenPlayerNotFoundExceptionIsThrown() {

        givenExistingGame(GAME_ID);
        givenUnexistingPlayer(PLAYER_NAME);

        final Throwable throwable =
            catchThrowable(() -> this.service.dealCardsToPlayer(GAME_ID, PLAYER_NAME, DEALT_CARDS_COUNT));

        assertThat(throwable).isInstanceOf(PlayerNotFoundException.class);
    }

    @Test
    public void givenExistingGameAndExistingPlayer_whenGettingPlayersCards_thenCardsAreReturned() {

        givenExistingGame(GAME_ID);
        givenExistingPlayer(PLAYER_NAME);
        givenPlayerHasCards(PLAYER_NAME, ANY_CARDS);

        final List<Card> cards = this.service.getPlayerCards(GAME_ID, PLAYER_NAME);

        assertThat(cards).containsExactlyElementsOf(ANY_CARDS);
    }

    @Test
    public void givenUnexistingGame_whenGettingPlayersCards_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.getPlayerCards(GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingPlayer_whenGettingPlayersCards_thenPlayerNotFoundExceptionIsThrown() {

        givenExistingGame(GAME_ID);
        givenUnexistingPlayer(PLAYER_NAME);

        final Throwable throwable = catchThrowable(() -> this.service.getPlayerCards(GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(PlayerNotFoundException.class);
    }

    @Test
    public void givenExistingGame_whenGettingSortedPlayers_thenPlayersAreReturnedSortedByHighestCardsFaceValueTotal() {

        givenExistingGame(GAME_ID);
        givenExistingPlayers(PLAYER_WITH_MIDDLE_TOTAL, PLAYER_WITH_LOWEST_TOTAL, PLAYER_WITH_HIGHEST_TOTAL);

        final List<Player> players = this.service.getSortedPlayers(GAME_ID);

        assertThat(players).extracting(Player::getName).containsExactly(PLAYER_WITH_HIGHEST_TOTAL.getName(),
            PLAYER_WITH_MIDDLE_TOTAL.getName(), PLAYER_WITH_LOWEST_TOTAL.getName());
    }

    @Test
    public void givenUnexistingGame_whenGettingSortedPlayers_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.getSortedPlayers(GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButNoPlayers_whenGettingSortedPlayers_thenNoPlayersAreReturned() {

        givenExistingGame(GAME_ID);

        final List<Player> players = this.service.getSortedPlayers(GAME_ID);

        assertThat(players).isEmpty();
    }

    @Test
    public void givenExistingGame_whenCountingRemainingCardsPerSuit_thenTotalPerSuitIsReturned() {

        givenExistingGame(GAME_ID);
        givenRemainingCardsInGame(CLUBS.card(12), SPADES.card(5), CLUBS.card(1), HEARTS.card(1), CLUBS.card(8),
            HEARTS.card(13), CLUBS.card(6));

        final Map<CardSuit, Long> remainingCardsCountPerSuit = this.service.countRemainingCardsPerSuit(GAME_ID);

        assertThat(remainingCardsCountPerSuit).containsOnly(entry(HEARTS, 2L), entry(SPADES, 1L), entry(CLUBS, 4L));
    }

    @Test
    public void givenExistingGameWithNoRemainingCards_whenCountingRemainingCardsPerSuit_thenNoTotalIsReturned() {

        givenExistingGame(GAME_ID);
        givenNoRemainingCardsInGame();

        final Map<CardSuit, Long> remainingCardsCountPerSuit = this.service.countRemainingCardsPerSuit(GAME_ID);

        assertThat(remainingCardsCountPerSuit).isEmpty();
    }

    @Test
    public void givenUnexistingGame_whenCountingRemainingCardsPerSuit_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.countRemainingCardsPerSuit(GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGame_whenCountingRemainingCardsPerCard_thenTotalPerCardIsReturned() {

        givenExistingGame(GAME_ID);
        givenRemainingCardsInGame(CLUBS.card(12), CLUBS.card(5), CLUBS.card(12), DIAMONDS.card(8), HEARTS.card(1),
            HEARTS.card(5), CLUBS.card(5), HEARTS.card(1), HEARTS.card(1), SPADES.card(5));

        final Map<Card, Long> remainingCardsCountPerSuit = this.service.countRemainingCardsPerCard(GAME_ID);

        assertThat(remainingCardsCountPerSuit).containsExactly(entry(HEARTS.card(5), 1L), entry(HEARTS.card(1), 3L),
            entry(SPADES.card(5), 1L), entry(CLUBS.card(12), 2L), entry(CLUBS.card(5), 2L),
            entry(DIAMONDS.card(8), 1L));
    }

    @Test
    public void givenExistingGameWithNoRemainingCards_whenCountingRemainingCardsPerCard_thenNoTotalIsReturned() {

        givenExistingGame(GAME_ID);
        givenNoRemainingCardsInGame();

        final Map<Card, Long> remainingCardsCountPerCard = this.service.countRemainingCardsPerCard(GAME_ID);

        assertThat(remainingCardsCountPerCard).isEmpty();
    }

    @Test
    public void givenUnexistingGame_whenCountingRemainingCardsPerCard_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.countRemainingCardsPerCard(GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGame_whenShufflingCards_thenCardsAreShuffled() {

        givenExistingGame(GAME_ID);

        this.service.shuffleCards(GAME_ID);

        verify(this.game).shuffleShoe();
    }

    @Test
    public void givenUnexistingGame_whenShufflingCards_thenGameNotFoundExceptionIsThrown() {

        givenUnexistingGame(GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.service.shuffleCards(GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    private void givenPlayerHasCards(
        final String playerName,
        final List<Card> cards) {

        given(this.game.getPlayerCards(PLAYER_NAME)).willReturn(cards);
    }

    private void givenUnexistingGame(
        final Integer gameId) {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.gameRepository).fetchById(gameId);
    }

    private void givenUnexistingDeck(
        final Integer deckId) {

        doThrow(DECK_NOT_FOUND_EXCEPTION).when(this.deckRepository).fetchById(deckId);
    }

    private void givenExistingGame(
        final Integer gameId) {

        given(this.gameRepository.fetchById(gameId)).willReturn(this.game);
    }

    private void givenExistingDeck(
        final Integer deckId) {

        given(this.deckRepository.fetchById(deckId)).willReturn(this.deck);
    }

    private void givenExistingPlayer(
        final String playerName) {

        given(this.game.containsPlayer(playerName)).willReturn(TRUE);
    }

    private void givenUnexistingPlayer(
        final String playerName) {

        given(this.game.containsPlayer(playerName)).willReturn(FALSE);
    }

    private void givenExistingPlayers(
        final Player... players) {

        final Set<Player> playersSet = Arrays.stream(players).collect(toSet());
        given(this.game.getPlayers()).willReturn(playersSet);
    }

    private void givenRemainingCardsInGame(
        final Card... cards) {

        given(this.game.allRemainingCardsInShoe()).willReturn(Arrays.asList(cards));
    }

    private void givenNoRemainingCardsInGame() {

        given(this.game.allRemainingCardsInShoe()).willReturn(Collections.emptyList());
    }

    private static Player createPlayerWithCardValues(
        final String playerName,
        final Integer... values) {

        final Player player = new CardPlayer(playerName);
        final List<Card> cards = Arrays.stream(values).map(HEARTS::card).collect(toList());

        player.addCards(cards);

        return player;
    }
}
