package com.example.domain;

import static com.example.domain.CardSuit.CLUBS;
import static com.example.domain.CardSuit.DIAMONDS;
import static com.example.domain.CardSuit.HEARTS;
import static com.example.domain.CardSuit.SPADES;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import java.util.List;

import org.junit.Test;

public final class CardSuitUT {

    public CardSuitUT() {

        super();
    }

    @Test
    public void whenGettingFullDeck_thenAllFiftyTwoCardsAreIncluded() {

        final List<Card> cards = CardSuit.fullDeckOfCards();

        assertThat(cards).extracting(Card::getSuit, Card::getFaceValue).containsExactly(tuple(HEARTS, 1),
            tuple(HEARTS, 2), tuple(HEARTS, 3), tuple(HEARTS, 4), tuple(HEARTS, 5), tuple(HEARTS, 6), tuple(HEARTS, 7),
            tuple(HEARTS, 8), tuple(HEARTS, 9), tuple(HEARTS, 10), tuple(HEARTS, 11), tuple(HEARTS, 12),
            tuple(HEARTS, 13), tuple(SPADES, 1), tuple(SPADES, 2), tuple(SPADES, 3), tuple(SPADES, 4), tuple(SPADES, 5),
            tuple(SPADES, 6), tuple(SPADES, 7), tuple(SPADES, 8), tuple(SPADES, 9), tuple(SPADES, 10),
            tuple(SPADES, 11), tuple(SPADES, 12), tuple(SPADES, 13), tuple(CLUBS, 1), tuple(CLUBS, 2), tuple(CLUBS, 3),
            tuple(CLUBS, 4), tuple(CLUBS, 5), tuple(CLUBS, 6), tuple(CLUBS, 7), tuple(CLUBS, 8), tuple(CLUBS, 9),
            tuple(CLUBS, 10), tuple(CLUBS, 11), tuple(CLUBS, 12), tuple(CLUBS, 13), tuple(DIAMONDS, 1),
            tuple(DIAMONDS, 2), tuple(DIAMONDS, 3), tuple(DIAMONDS, 4), tuple(DIAMONDS, 5), tuple(DIAMONDS, 6),
            tuple(DIAMONDS, 7), tuple(DIAMONDS, 8), tuple(DIAMONDS, 9), tuple(DIAMONDS, 10), tuple(DIAMONDS, 11),
            tuple(DIAMONDS, 12), tuple(DIAMONDS, 13));
    }
}
