package com.example.domain;

import static com.example.domain.CardSuit.CLUBS;
import static com.example.domain.CardSuit.HEARTS;
import static com.example.domain.CardSuit.SPADES;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;

public final class CardPlayerUT {

    private static final String PLAYER_NAME = "Steeve";

    private static final List<Card> ANY_TWO_CARDS = Lists.newArrayList(HEARTS.card(3), SPADES.card(12));

    private Player player;

    public CardPlayerUT() {

        super();
    }

    @Before
    public void initialize() {

        this.player = new CardPlayer(PLAYER_NAME);
    }

    @Test
    public void whenCreatingPlayer_thenHasExpectedName() {

        assertThat(this.player.getName()).isEqualTo(PLAYER_NAME);
    }

    @Test
    public void whenCreatingPlayer_thenPlayerHasNoCards() {

        assertThat(this.player.getCards()).isEmpty();
    }

    @Test
    public void whenAddingCardsToPlayer_thenCardsAreSuccessfullyAdded() {

        this.player.addCards(ANY_TWO_CARDS);

        assertThat(this.player.getCards()).containsOnlyElementsOf(ANY_TWO_CARDS);
    }

    @Test
    public void whenAddingCardsMultipleTimesToPlayer_thenAllCardsAreSuccessfullyAdded() {

        final Card first = CLUBS.card(3);
        final Card second = SPADES.card(3);
        final Card third = HEARTS.card(3);

        this.player.addCards(Lists.list(first));
        this.player.addCards(Lists.list(second));
        this.player.addCards(Lists.list(third));

        assertThat(this.player.getCards()).containsExactlyInAnyOrder(first, second, third);
    }

    @Test
    public void givenPlayerHasNoCards_whenGettingTotalFaceValue_thenZeroIsReturned() {

        final Integer totalFaceValue = this.player.getCardsTotalFaceValue();

        assertThat(totalFaceValue).isEqualTo(0);
    }

    @Test
    public void givenPlayerHasCards_whenGettingTotalFaceValue_thenTotalIsReturned() {

        givenPlayerHasCard(HEARTS, 2);
        givenPlayerHasCard(SPADES, 8);
        givenPlayerHasCard(HEARTS, 13);

        final Integer totalFaceValue = this.player.getCardsTotalFaceValue();

        assertThat(totalFaceValue).isEqualTo(23);
    }

    private void givenPlayerHasCard(
        final CardSuit suit,
        final Integer faceValue) {

        this.player.addCards(Lists.list(suit.card(faceValue)));
    }
}
