package com.example.domain;

import static com.example.domain.CardSuit.CLUBS;
import static com.example.domain.CardSuit.DIAMONDS;
import static com.example.domain.CardSuit.HEARTS;
import static com.example.domain.CardSuit.SPADES;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public final class ShoeUT {

    private static final Integer FIRST_DECK_ID = 3;

    private static final Integer SECOND_DECK_ID = 2;

    private static final Integer THIRD_DECK_ID = 5;

    private Shoe shoe;

    public ShoeUT() {

        super();
    }

    @Before
    public void initialize() {

        this.shoe = new Shoe();
    }

    @Test
    public void whenAddingDeck_thenDeckIsAdded() {

        final Deck deck = new CardDeck(FIRST_DECK_ID);

        this.shoe.addDeck(deck);

        assertThat(this.shoe.containsDeck(FIRST_DECK_ID)).isTrue();
    }

    @Test
    public void givenMultipleDecks_whenAddingThemAll_thenAllDecksAreAvailableInAddedOrder() {

        final Deck first = new CardDeck(FIRST_DECK_ID);
        final Deck second = new CardDeck(SECOND_DECK_ID);
        final Deck third = new CardDeck(THIRD_DECK_ID);

        this.shoe.addDeck(first);
        this.shoe.addDeck(second);
        this.shoe.addDeck(third);

        assertThat(this.shoe.getAllDecks()).extracting(Deck::getId).containsExactly(FIRST_DECK_ID, SECOND_DECK_ID,
            THIRD_DECK_ID);
    }

    @Test
    public void givenAlreadyAddedDeck_whenAddingTheSameDeckAgain_thenNothingIsAdded() {

        givenExistingDeck(FIRST_DECK_ID);
        final Deck deck = new CardDeck(FIRST_DECK_ID);

        this.shoe.addDeck(deck);

        assertThat(this.shoe.getAllDecks()).extracting(Deck::getId).containsExactly(FIRST_DECK_ID);
    }

    @Test
    public void givenMultipleDecks_whenGettingShoeRemainingCards_thenTotalIsTheSumOfAllDecks() {

        givenExistingDeck(FIRST_DECK_ID);
        givenExistingDeck(SECOND_DECK_ID);
        givenExistingDeck(THIRD_DECK_ID);

        final Integer remainingCards = this.shoe.remainingCardsCount();

        assertThat(remainingCards).isEqualTo(156);
    }

    @Test
    public void givenExistingDeck_whenDealingCardsMultipleTimes_thenRemainingCardsCountInShoeIsCorrectlyDecremented() {

        givenExistingDeck(FIRST_DECK_ID);

        this.shoe.dealCards(5);
        this.shoe.dealCards(8);
        this.shoe.dealCards(1);

        assertThat(this.shoe.remainingCardsCount()).isEqualTo(38);
    }

    @Test
    public void givenNoDecks_whenDealingCards_thenNoCardsAreReturned() {

        final List<Card> cards = this.shoe.dealCards(3);

        assertThat(cards).isEmpty();
    }

    @Test
    public void givenExistingDeckWithNoRemainingCards_whenDealingMoreCards_thenNoCardsAreReturned() {

        givenExistingDeck(FIRST_DECK_ID);
        this.shoe.dealCards(52);

        this.shoe.dealCards(3);

        assertThat(this.shoe.remainingCardsCount()).isEqualTo(0);
    }

    @Test
    public void givenMultipleDecks_whenDealingMoreCardsThanSingleDeckCanContain_thenCardsAreDealtFromNextDeck() {

        givenExistingDeck(FIRST_DECK_ID);
        givenExistingDeck(SECOND_DECK_ID);

        this.shoe.dealCards(60);

        assertThat(this.shoe.remainingCardsCount()).isEqualTo(44);
    }

    @Test
    public void givenMultipleDecks_whenDealingMoreCardsThanTotalOfAllDecks_thenNoMoreCardsThanTotalAreReturned() {

        givenExistingDeck(FIRST_DECK_ID);
        givenExistingDeck(SECOND_DECK_ID);

        final List<Card> cards = this.shoe.dealCards(108);

        assertThat(this.shoe.remainingCardsCount()).isEqualTo(0);
        assertThat(cards).hasSize(104);
    }

    @Test
    public void givenNoDecks_whenGettingRemainingCards_thenNoCardsAreReturned() {

        final List<Card> cards = this.shoe.remainingCards();

        assertThat(cards).hasSize(0);
    }

    @Test
    public void givenMultipleDecks_whenGettingRemainingCards_thenCardsFromAllDecksAreReturned() {

        givenExistingDeck(FIRST_DECK_ID);
        givenExistingDeck(SECOND_DECK_ID);

        final List<Card> cards = this.shoe.remainingCards();

        assertThat(cards).hasSize(104);
    }

    @Test
    public void givenMultipleDecks_whenShuffling_thenAllDecksAreShuffled() {

        givenExistingDeck(FIRST_DECK_ID);
        givenExistingDeck(SECOND_DECK_ID);

        this.shoe.shuffle();

        final List<Card> cards = this.shoe.remainingCards();
        assertThat(cards).doesNotContainSequence(HEARTS.card(1), HEARTS.card(2), HEARTS.card(3), HEARTS.card(4));
        assertThat(cards).doesNotContainSequence(SPADES.card(1), SPADES.card(2), SPADES.card(3), SPADES.card(4));
        assertThat(cards).doesNotContainSequence(DIAMONDS.card(1), DIAMONDS.card(2), DIAMONDS.card(3),
            DIAMONDS.card(4));
        assertThat(cards).doesNotContainSequence(CLUBS.card(1), CLUBS.card(2), CLUBS.card(3), CLUBS.card(4));
    }

    private void givenExistingDeck(
        final Integer deckId) {

        final Deck first = new CardDeck(deckId);
        this.shoe.addDeck(first);
    }

}
