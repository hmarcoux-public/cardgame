package com.example.domain;

public interface GameRepository {

    Game createGame();

    Game fetchById(
        Integer id);

    void delete(
        Integer id);

    void clearAll();
}
