package com.example.domain;

import java.util.List;

public interface Deck {

    Integer getId();

    List<Card> dealCards(
        Integer count);

    List<Card> remainingCards();

    boolean hasRemainingCards();

    void shuffle();
}
