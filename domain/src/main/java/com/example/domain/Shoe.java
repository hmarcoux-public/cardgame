package com.example.domain;

import static java.util.stream.Collectors.toList;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class Shoe {

    private final Set<Deck> decks;

    public Shoe() {

        this.decks = new LinkedHashSet<>();
    }

    public void addDeck(
        final Deck deck) {

        this.decks.add(deck);
    }

    List<Deck> getAllDecks() {

        return new ArrayList<>(this.decks);
    }

    public boolean containsDeck(
        final Integer deckId) {

        return this.decks.stream().map(Deck::getId).collect(toList()).contains(deckId);
    }

    public Integer remainingCardsCount() {

        return this.decks.stream().map(Deck::remainingCards).map(List::size).reduce(0, Integer::sum);
    }

    public List<Card> dealCards(
        final Integer cardsCount) {

        final List<Card> allDealtCards = new ArrayList<>();
        int remainingCardsToDeal = cardsCount;

        while (remainingCardsToDeal > 0 && hasRemainingCards()) {

            final Deck deck = this.decks.stream().filter(Deck::hasRemainingCards).findFirst().get();
            final List<Card> currentlyDealtCards = deck.dealCards(remainingCardsToDeal);

            allDealtCards.addAll(currentlyDealtCards);

            remainingCardsToDeal = remainingCardsToDeal - currentlyDealtCards.size();
        }

        return allDealtCards;
    }

    private boolean hasRemainingCards() {

        return remainingCardsCount() > 0;
    }

    public List<Card> remainingCards() {

        return this.decks.stream().map(Deck::remainingCards).flatMap(List::stream).collect(toList());
    }

    public void shuffle() {

        this.decks.forEach(Deck::shuffle);
    }
}
