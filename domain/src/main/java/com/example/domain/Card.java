package com.example.domain;

public interface Card {

    Integer getFaceValue();

    CardSuit getSuit();
}
