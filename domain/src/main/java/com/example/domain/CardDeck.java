package com.example.domain;

import static java.util.stream.Collectors.toList;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.stream.IntStream;

public final class CardDeck
    implements Deck {

    private final Integer id;

    private final Deque<Card> cards;

    public CardDeck(
        final Integer id) {

        this.id = id;
        this.cards = new ArrayDeque<>(CardSuit.fullDeckOfCards());
    }

    @Override
    public Integer getId() {

        return this.id;
    }

    @Override
    public List<Card> dealCards(
        final Integer count) {

        final int countOfDealableCards = count > this.cards.size() ? this.cards.size() : count;

        return IntStream.rangeClosed(1, countOfDealableCards).mapToObj(i -> this.cards.pop()).collect(toList());
    }

    @Override
    public List<Card> remainingCards() {

        return new ArrayList<>(this.cards);
    }

    @Override
    public boolean hasRemainingCards() {

        return this.cards.size() > 0;
    }

    @Override
    public void shuffle() {

        final List<Card> shuffledCards = this.cards.stream().sorted(new CardRandomComparator()).collect(toList());

        this.cards.clear();
        this.cards.addAll(shuffledCards);
    }

    @Override
    public int hashCode() {

        return Objects.hash(this.id);
    }

    @Override
    public boolean equals(
        final Object other) {

        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        final Deck otherDeck = (Deck) other;

        return Objects.equals(otherDeck.getId(), this.id);
    }

    private static class CardRandomComparator
        implements Comparator<Card> {

        private final Map<Card, Integer> map = new IdentityHashMap<>();

        private final Random random;

        CardRandomComparator() {

            this.random = new Random();
        }

        private int valueFor(
            final Card card) {

            return this.map.computeIfAbsent(card, ignore -> this.random.nextInt());
        }

        @Override
        public int compare(
            final Card first,
            final Card other) {

            return Integer.compare(valueFor(first), valueFor(other));
        }
    }
}
