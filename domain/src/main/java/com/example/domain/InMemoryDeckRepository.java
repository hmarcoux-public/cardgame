package com.example.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import com.example.domain.exceptions.DeckNotFoundException;

public final class InMemoryDeckRepository
    implements DeckRepository {

    private final Map<Integer, Deck> decksById;

    private final AtomicInteger idAutoIncrementer;

    public InMemoryDeckRepository() {

        this.decksById = new HashMap<>();
        this.idAutoIncrementer = new AtomicInteger();
    }

    @Override
    public Deck createDeck() {

        final Integer nextId = this.idAutoIncrementer.incrementAndGet();
        final Deck deck = new CardDeck(nextId);

        this.decksById.put(nextId, deck);

        return deck;
    }

    @Override
    public Deck fetchById(
        final Integer id) {

        return Optional.ofNullable(this.decksById.get(id)).orElseThrow(() -> new DeckNotFoundException(id));
    }

    @Override
    public void clearAll() {

        this.decksById.clear();
        this.idAutoIncrementer.set(0);
    }

}
