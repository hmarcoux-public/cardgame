package com.example.domain;

import java.util.List;

public interface Player {

    String getName();

    void addCards(
        List<Card> cards);

    List<Card> getCards();

    Integer getCardsTotalFaceValue();
}
