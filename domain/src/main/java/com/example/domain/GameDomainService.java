package com.example.domain;

import java.util.List;
import java.util.Map;

public interface GameDomainService {

    void addDeckToGame(
        Integer gameId,
        Integer deckId);

    void addPlayerToGame(
        Integer gameId,
        String playerName);

    void removePlayerFromGame(
        Integer gameId,
        String playerName);

    void dealCardsToPlayer(
        Integer gameId,
        String playerName,
        Integer cardsCount);

    List<Card> getPlayerCards(
        Integer gameId,
        String playerName);

    List<Player> getSortedPlayers(
        Integer gameId);

    Map<CardSuit, Long> countRemainingCardsPerSuit(
        Integer gameId);

    Map<Card, Long> countRemainingCardsPerCard(
        Integer gameId);

    void shuffleCards(
        Integer gameId);
}
