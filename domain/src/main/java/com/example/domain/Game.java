package com.example.domain;

import java.util.List;
import java.util.Set;

public interface Game {

    Integer getId();

    void addDeck(
        Deck deck);

    boolean containsDeck(
        Integer deckId);

    Integer remainingCardsCountInShoe();

    List<Card> allRemainingCardsInShoe();

    void shuffleShoe();

    void addPlayer(
        String playerName);

    Player getPlayer(
        String playerName);

    Set<Player> getPlayers();

    void removePlayer(
        String playerName);

    boolean containsPlayer(
        String playerName);

    void dealCardsToPlayer(
        String playerName,
        Integer cardsCount);

    List<Card> getPlayerCards(
        String playerName);
}
