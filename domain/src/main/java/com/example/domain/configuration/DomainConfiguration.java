package com.example.domain.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.example.domain.CardGameDomainService;
import com.example.domain.DeckRepository;
import com.example.domain.GameDomainService;
import com.example.domain.GameRepository;
import com.example.domain.InMemoryDeckRepository;
import com.example.domain.InMemoryGameRepository;

@Configuration
public class DomainConfiguration {

    public DomainConfiguration() {

        super();
    }

    @Bean
    public GameDomainService gameDomainService() {

        return new CardGameDomainService();
    }

    @Bean
    public GameRepository gameRepository() {

        return new InMemoryGameRepository();
    }

    @Bean
    public DeckRepository deckRepository() {

        return new InMemoryDeckRepository();
    }
}
