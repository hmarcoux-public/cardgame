package com.example.domain;

public interface DeckRepository {

    Deck createDeck();

    Deck fetchById(
        Integer id);

    void clearAll();
}
