package com.example.domain;

import static java.util.stream.Collectors.toList;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.IntStream;

public enum CardSuit {

    HEARTS(60),
    SPADES(40),
    CLUBS(20),
    DIAMONDS(0);

    private final Integer orderWeight;

    CardSuit(
        final Integer orderWeight) {

        this.orderWeight = orderWeight;
    }

    public List<Card> allSuitCards() {

        return IntStream.rangeClosed(1, 13).mapToObj(faceValue -> new DefaultCard(faceValue, this)).collect(toList());
    }

    public static List<Card> fullDeckOfCards() {

        return Arrays.stream(values()).map(CardSuit::allSuitCards).flatMap(List::stream).collect(toList());
    }

    public Card card(
        final Integer faceValue) {

        return new DefaultCard(faceValue, this);
    }

    Integer getOrderWeight() {

        return this.orderWeight;
    }

    private static class DefaultCard
        implements Card, Comparable<Card> {

        private final Integer faceValue;

        private final CardSuit suit;

        private DefaultCard(
            final Integer faceValue,
            final CardSuit suit) {

            this.faceValue = faceValue;
            this.suit = suit;
        }

        @Override
        public Integer getFaceValue() {

            return this.faceValue;
        }

        @Override
        public CardSuit getSuit() {

            return this.suit;
        }

        @Override
        public int hashCode() {

            return Objects.hash(this.suit, this.faceValue);
        }

        @Override
        public boolean equals(
            final Object other) {

            if (this == other) {
                return true;
            }

            if (other == null || getClass() != other.getClass()) {
                return false;
            }

            final Card otherCard = (Card) other;

            return Objects.equals(otherCard.getSuit(), this.suit)
                && Objects.equals(otherCard.getFaceValue(), this.faceValue);
        }

        @Override
        public String toString() {

            return this.suit.name() + "-" + this.faceValue.toString();
        }

        @Override
        public int compareTo(
            final Card other) {

            return orderWeight().compareTo(other.getSuit().getOrderWeight() + other.getFaceValue());
        }

        private Integer orderWeight() {

            return this.suit.getOrderWeight() + this.faceValue;
        }
    }
}
