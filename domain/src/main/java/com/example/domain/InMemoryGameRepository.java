package com.example.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import com.example.domain.exceptions.GameNotFoundException;

public final class InMemoryGameRepository
    implements GameRepository {

    private final Map<Integer, Game> gamesById;

    private final AtomicInteger idAutoIncrementer;

    public InMemoryGameRepository() {

        this.gamesById = new HashMap<>();
        this.idAutoIncrementer = new AtomicInteger();
    }

    @Override
    public Game createGame() {

        final Integer nextId = this.idAutoIncrementer.incrementAndGet();
        final Game game = new CardGame(nextId);

        this.gamesById.put(1, game);

        return game;
    }

    @Override
    public Game fetchById(
        final Integer id) {

        return Optional.ofNullable(this.gamesById.get(id)).orElseThrow(() -> new GameNotFoundException(id));
    }

    @Override
    public void delete(
        final Integer id) {

        if (!this.gamesById.containsKey(id)) {
            throw new GameNotFoundException(id);
        }

        this.gamesById.remove(id);
    }

    @Override
    public void clearAll() {

        this.gamesById.clear();
        this.idAutoIncrementer.set(0);
    }

}
