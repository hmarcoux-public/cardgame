package com.example.domain;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public final class CardGame
    implements Game {

    private final Integer id;

    private final Shoe shoe;

    private final Map<String, Player> playersByName;

    public CardGame(
        final Integer id) {

        this.id = id;
        this.shoe = new Shoe();
        this.playersByName = new HashMap<>();
    }

    @Override
    public Integer getId() {

        return this.id;
    }

    @Override
    public void addDeck(
        final Deck deck) {

        this.shoe.addDeck(deck);
    }

    @Override
    public boolean containsDeck(
        final Integer deckId) {

        return this.shoe.containsDeck(deckId);
    }

    @Override
    public Integer remainingCardsCountInShoe() {

        return this.shoe.remainingCardsCount();
    }

    @Override
    public List<Card> allRemainingCardsInShoe() {

        return this.shoe.remainingCards();
    }

    @Override
    public void shuffleShoe() {

        this.shoe.shuffle();
    }

    @Override
    public void addPlayer(
        final String playerName) {

        this.playersByName.put(playerName, new CardPlayer(playerName));
    }

    @Override
    public Player getPlayer(
        final String playerName) {

        return this.playersByName.get(playerName);
    }

    @Override
    public Set<Player> getPlayers() {

        return new HashSet<>(this.playersByName.values());
    }

    @Override
    public void removePlayer(
        final String playerName) {

        this.playersByName.remove(playerName);
    }

    @Override
    public boolean containsPlayer(
        final String playerName) {

        return this.playersByName.containsKey(playerName);
    }

    @Override
    public void dealCardsToPlayer(
        final String playerName,
        final Integer cardsCount) {

        if (!containsPlayer(playerName)) {
            return;
        }

        final List<Card> cards = this.shoe.dealCards(cardsCount);
        getPlayer(playerName).addCards(cards);
    }

    @Override
    public List<Card> getPlayerCards(
        final String playerName) {

        if (!containsPlayer(playerName)) {
            return Collections.emptyList();
        }

        return getPlayer(playerName).getCards();
    }
}
