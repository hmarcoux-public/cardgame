package com.example.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public final class CardPlayer
    implements Player {

    private final String name;

    private final List<Card> cards;

    public CardPlayer(
        final String name) {

        this.name = name;
        this.cards = new ArrayList<>();
    }

    @Override
    public String getName() {

        return this.name;
    }

    @Override
    public void addCards(
        final List<Card> cardsToAdd) {

        this.cards.addAll(cardsToAdd);
    }

    @Override
    public List<Card> getCards() {

        return this.cards;
    }

    @Override
    public Integer getCardsTotalFaceValue() {

        return this.cards.stream().map(Card::getFaceValue).reduce(0, Integer::sum);
    }

    @Override
    public int hashCode() {

        return Objects.hash(this.name);
    }

    @Override
    public boolean equals(
        final Object other) {

        if (this == other) {
            return true;
        }

        if (other == null || getClass() != other.getClass()) {
            return false;
        }

        final Player otherPlayer = (Player) other;

        return Objects.equals(otherPlayer.getName(), this.name);
    }
}
