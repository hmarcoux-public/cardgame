package com.example.domain;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.domain.exceptions.PlayerNotFoundException;

public final class CardGameDomainService
    implements GameDomainService {

    @Autowired
    private DeckRepository deckRepository;

    @Autowired
    private GameRepository gameRepository;

    public CardGameDomainService() {

        super();
    }

    @Override
    public void addDeckToGame(
        final Integer gameId,
        final Integer deckId) {

        final Game game = this.gameRepository.fetchById(gameId);
        final Deck deck = this.deckRepository.fetchById(deckId);

        game.addDeck(deck);
    }

    @Override
    public void addPlayerToGame(
        final Integer gameId,
        final String playerName) {

        final Game game = this.gameRepository.fetchById(gameId);

        game.addPlayer(playerName);
    }

    @Override
    public void removePlayerFromGame(
        final Integer gameId,
        final String playerName) {

        final Game game = this.gameRepository.fetchById(gameId);

        if (!game.containsPlayer(playerName)) {
            throw new PlayerNotFoundException(playerName);
        }

        game.removePlayer(playerName);
    }

    @Override
    public void dealCardsToPlayer(
        final Integer gameId,
        final String playerName,
        final Integer cardsCount) {

        final Game game = this.gameRepository.fetchById(gameId);

        if (!game.containsPlayer(playerName)) {
            throw new PlayerNotFoundException(playerName);
        }

        game.dealCardsToPlayer(playerName, cardsCount);
    }

    @Override
    public List<Card> getPlayerCards(
        final Integer gameId,
        final String playerName) {

        final Game game = this.gameRepository.fetchById(gameId);

        if (!game.containsPlayer(playerName)) {
            throw new PlayerNotFoundException(playerName);
        }

        return game.getPlayerCards(playerName);
    }

    @Override
    public List<Player> getSortedPlayers(
        final Integer gameId) {

        final Game game = this.gameRepository.fetchById(gameId);

        return game.getPlayers().stream().sorted(Comparator.comparingInt(Player::getCardsTotalFaceValue).reversed())
            .collect(toList());
    }

    @Override
    public Map<CardSuit, Long> countRemainingCardsPerSuit(
        final Integer gameId) {

        final Game game = this.gameRepository.fetchById(gameId);
        final List<Card> remainingCards = game.allRemainingCardsInShoe();

        return remainingCards.stream().collect(groupingBy(Card::getSuit, counting()));
    }

    @Override
    public Map<Card, Long> countRemainingCardsPerCard(
        final Integer gameId) {

        final Game game = this.gameRepository.fetchById(gameId);
        final List<Card> remainingCards = game.allRemainingCardsInShoe();

        final Map<Card, Long> unsortedRemainingCards =
            remainingCards.stream().collect(groupingBy(Function.identity(), counting()));

        final Map<Card, Long> sortedRemainingCards = new TreeMap<>(Collections.reverseOrder());
        sortedRemainingCards.putAll(unsortedRemainingCards);

        return sortedRemainingCards;
    }

    @Override
    public void shuffleCards(
        final Integer gameId) {

        final Game game = this.gameRepository.fetchById(gameId);

        game.shuffleShoe();
    }
}
