package com.example.domain.exceptions;

public final class DeckNotFoundException
    extends BusinessException {

    private static final long serialVersionUID = -4850002885644088580L;

    public DeckNotFoundException(
        final Integer deckId) {

        super(String.format("Deck with id %d could not be found", deckId), 404);
    }
}
