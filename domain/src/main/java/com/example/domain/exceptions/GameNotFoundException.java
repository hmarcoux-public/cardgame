package com.example.domain.exceptions;

public final class GameNotFoundException
    extends BusinessException {

    private static final long serialVersionUID = -7026909544422942978L;

    public GameNotFoundException(
        final Integer gameId) {

        super(String.format("Game with id %d could not be found", gameId), 404);
    }
}
