package com.example.domain.exceptions;

public final class PlayerNotFoundException
    extends BusinessException {

    private static final long serialVersionUID = -5735443884212370026L;

    public PlayerNotFoundException(
        final String playerName) {

        super(String.format("Player with name %s could not be found", playerName), 404);
    }
}
