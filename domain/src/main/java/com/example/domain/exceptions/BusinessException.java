package com.example.domain.exceptions;

public abstract class BusinessException
    extends RuntimeException {

    private static final long serialVersionUID = 7909938135011476487L;

    private final Integer errorCode;

    public BusinessException(
        final String message,
        final Integer errorCode) {

        super(message);

        this.errorCode = errorCode;
    }

    public Integer getErrorCode() {

        return this.errorCode;
    }
}
