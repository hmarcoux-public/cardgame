package com.example.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com.example")
public class Application {

    protected Application() {

        super();
    }

    public static void main(
        final String[] args) {

        SpringApplication.run(Application.class, args);
    }
}
