package com.example.service.controllers.messaging;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class DealCardsRequest
    implements Serializable {

    private static final long serialVersionUID = -3836735908957958493L;

    @JsonProperty("count")
    private Integer cardsCount;

    public DealCardsRequest() {

        // Empty constructor for deserialization
    }

    public DealCardsRequest(
        final Integer cardsCount) {

        this.cardsCount = cardsCount;
    }

    public Integer getCount() {

        return this.cardsCount;
    }
}
