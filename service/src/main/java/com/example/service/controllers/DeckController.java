package com.example.service.controllers;

import static javax.ws.rs.core.Response.Status.CREATED;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.domain.Deck;
import com.example.domain.DeckRepository;
import com.example.service.controllers.messaging.CreateDeckResponse;

@Path("/decks")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public final class DeckController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeckController.class);

    @Autowired
    private DeckRepository repository;

    public DeckController() {

        super();
    }

    @POST
    public Response createDeck() {

        final Deck deck = this.repository.createDeck();
        final CreateDeckResponse response = new CreateDeckResponse(deck.getId());

        LOGGER.info(String.format("Deck %d successfully created", deck.getId()));

        return Response.status(CREATED).entity(response).build();
    }
}
