package com.example.service.controllers.messaging;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class JsonCardCount {

    @JsonProperty("card")
    private JsonCard card;

    @JsonProperty("count")
    private Long count;

    public JsonCardCount() {

        // Empty constructor for deserialization
    }

    public JsonCardCount(
        final JsonCard card,
        final Long count) {

        this.card = card;
        this.count = count;
    }

    public JsonCard getCard() {

        return this.card;
    }

    public Long getCount() {

        return this.count;
    }
}
