package com.example.service.controllers.messaging;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.List;

import com.example.domain.Player;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class PlayerSummaryResponse
    implements Serializable {

    private static final long serialVersionUID = -4461647265779744598L;

    @JsonProperty("players")
    private List<JsonPlayerSummary> playerSummaries;

    public PlayerSummaryResponse() {

        // Empty constructor for deserialization
    }

    private PlayerSummaryResponse(
        final List<JsonPlayerSummary> playerSummaries) {

        this.playerSummaries = playerSummaries;
    }

    public static PlayerSummaryResponse fromPlayers(
        final List<Player> players) {

        final List<JsonPlayerSummary> jsonPlayers =
            players.stream().map(PlayerSummaryResponse::toJsonPlayer).collect(toList());
        return new PlayerSummaryResponse(jsonPlayers);
    }

    private static JsonPlayerSummary toJsonPlayer(
        final Player player) {

        return new JsonPlayerSummary(player.getName(), player.getCardsTotalFaceValue());
    }

}
