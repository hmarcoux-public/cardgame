package com.example.service.controllers.messaging;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.example.domain.CardSuit;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class CardsSummaryResponse
    implements Serializable {

    private static final long serialVersionUID = 5929356516517629004L;

    @JsonProperty("cards")
    private List<JsonCardSummary> cardSummaries;

    public CardsSummaryResponse() {

        // Empty constructor for deserialization
    }

    private CardsSummaryResponse(
        final List<JsonCardSummary> cardSummaries) {

        this.cardSummaries = cardSummaries;
    }

    public static CardsSummaryResponse fromCards(
        final Map<CardSuit, Long> cards) {

        final List<JsonCardSummary> cardSummaries =
            cards.entrySet().stream().map(CardsSummaryResponse::toJsonCard).collect(toList());
        return new CardsSummaryResponse(cardSummaries);
    }

    private static JsonCardSummary toJsonCard(
        final Map.Entry<CardSuit, Long> cardSummary) {

        return new JsonCardSummary(cardSummary.getKey().name().toLowerCase(), cardSummary.getValue());
    }
}
