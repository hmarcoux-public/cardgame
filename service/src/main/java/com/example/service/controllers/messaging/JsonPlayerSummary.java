package com.example.service.controllers.messaging;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class JsonPlayerSummary {

    @JsonProperty("name)")
    private String name;

    @JsonProperty("total_face_value")
    private Integer totalFaceValue;

    public JsonPlayerSummary() {

        // Empty constructor for deserialization
    }

    public JsonPlayerSummary(
        final String name,
        final Integer totalFaceValue) {

        this.name = name;
        this.totalFaceValue = totalFaceValue;
    }

    public String getName() {

        return this.name;
    }

    public Integer getTotalFaceValue() {

        return this.totalFaceValue;
    }
}
