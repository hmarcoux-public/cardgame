package com.example.service.controllers.messaging;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class CreateGameResponse
    implements Serializable {

    private static final long serialVersionUID = 755128560883974531L;

    @JsonProperty("id")
    private Integer gameId;

    public CreateGameResponse() {

        // Empty constructor for deserialization
    }

    public CreateGameResponse(
        final Integer gameId) {

        this.gameId = gameId;
    }

    public Integer getGameId() {

        return this.gameId;
    }
}
