package com.example.service.controllers.messaging;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class JsonCardSummary {

    @JsonProperty("suit")
    private String suit;

    @JsonProperty("count")
    private Long count;

    public JsonCardSummary() {

        // Empty constructor for deserialization
    }

    public JsonCardSummary(
        final String suit,
        final Long count) {

        this.suit = suit;
        this.count = count;
    }

    public String getSuit() {

        return this.suit;
    }

    public Long getCount() {

        return this.count;
    }
}
