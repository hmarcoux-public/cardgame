package com.example.service.controllers.messaging;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.List;

import com.example.domain.Card;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class CardsResponse
    implements Serializable {

    private static final long serialVersionUID = 67487576589511042L;

    @JsonProperty("cards")
    private List<JsonCard> cards;

    public CardsResponse() {

        // Empty constructor for deserialization
    }

    private CardsResponse(
        final List<JsonCard> jsonCards) {

        this.cards = jsonCards;
    }

    public static CardsResponse fromCards(
        final List<Card> cards) {

        return new CardsResponse(cards.stream().map(JsonCard::fromCard).collect(toList()));
    }

    public List<JsonCard> getCards() {

        return this.cards;
    }
}
