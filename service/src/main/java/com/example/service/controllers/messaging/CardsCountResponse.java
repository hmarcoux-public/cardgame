package com.example.service.controllers.messaging;

import static java.util.stream.Collectors.toList;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.example.domain.Card;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class CardsCountResponse
    implements Serializable {

    private static final long serialVersionUID = 5929356516517629004L;

    @JsonProperty("cards")
    private List<JsonCardCount> cardCounts;

    public CardsCountResponse() {

        // Empty constructor for deserialization
    }

    private CardsCountResponse(
        final List<JsonCardCount> cardCounts) {

        this.cardCounts = cardCounts;
    }

    public static CardsCountResponse fromCards(
        final Map<Card, Long> cards) {

        final List<JsonCardCount> jsonCardCounts =
            cards.entrySet().stream().map(CardsCountResponse::toJsonCard).collect(toList());
        return new CardsCountResponse(jsonCardCounts);
    }

    private static JsonCardCount toJsonCard(
        final Map.Entry<Card, Long> cardCounts) {

        final JsonCard jsonCard = JsonCard.fromCard(cardCounts.getKey());
        return new JsonCardCount(jsonCard, cardCounts.getValue());
    }
}
