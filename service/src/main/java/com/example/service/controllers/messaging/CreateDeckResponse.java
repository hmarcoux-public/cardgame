package com.example.service.controllers.messaging;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public final class CreateDeckResponse
    implements Serializable {

    private static final long serialVersionUID = 2463867901675315033L;

    @JsonProperty("id")
    private Integer deckId;

    public CreateDeckResponse() {

        // Empty constructor for deserialization
    }

    public CreateDeckResponse(
        final Integer deckId) {

        this.deckId = deckId;
    }

    public Integer getDeckId() {

        return this.deckId;
    }
}
