package com.example.service.controllers.messaging;

import com.example.domain.Card;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class JsonCard {

    @JsonProperty("suit")
    private String suit;

    @JsonProperty("face_value")
    private Integer faceValue;

    public JsonCard() {

        // Empty constructor for deserialization
    }

    private JsonCard(
        final String suit,
        final Integer faceValue) {

        this.suit = suit;
        this.faceValue = faceValue;
    }

    public static JsonCard fromCard(
        final Card card) {

        return new JsonCard(card.getSuit().name().toLowerCase(), card.getFaceValue());
    }

    public String getSuit() {

        return this.suit;
    }

    public Integer getFaceValue() {

        return this.faceValue;
    }
}
