package com.example.service.controllers;

import static javax.ws.rs.core.Response.Status.CREATED;

import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.domain.Card;
import com.example.domain.CardSuit;
import com.example.domain.Game;
import com.example.domain.GameDomainService;
import com.example.domain.GameRepository;
import com.example.domain.Player;
import com.example.service.controllers.messaging.CardsCountResponse;
import com.example.service.controllers.messaging.CardsResponse;
import com.example.service.controllers.messaging.CardsSummaryResponse;
import com.example.service.controllers.messaging.CreateGameResponse;
import com.example.service.controllers.messaging.PlayerSummaryResponse;

@Path("/games")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Component
public final class GameController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameController.class);

    @Autowired
    private GameRepository repository;

    @Autowired
    private GameDomainService service;

    public GameController() {

        super();
    }

    @POST
    public Response createGame() {

        final Game game = this.repository.createGame();
        final CreateGameResponse response = new CreateGameResponse(game.getId());

        LOGGER.info(String.format("Successfully created game %d", game.getId()));

        return Response.status(CREATED).entity(response).build();
    }

    @DELETE
    @Path("/{gameId}")
    public Response deleteGame(
        @PathParam("gameId") final Integer gameId) {

        this.repository.delete(gameId);

        LOGGER.info(String.format("Successfully deleted game %d", gameId));

        return Response.noContent().build();
    }

    @PUT
    @Path("/{gameId}/decks/{deckId}")
    public Response addDeckToGame(
        @PathParam("gameId") final Integer gameId,
        @PathParam("deckId") final Integer deckId) {

        this.service.addDeckToGame(gameId, deckId);

        LOGGER.info(String.format("Successfully added deck %d to game %d", deckId, gameId));

        return Response.noContent().build();
    }

    @PUT
    @Path("/{gameId}/players/{playerName}")
    public Response addPlayerToGame(
        @PathParam("gameId") final Integer gameId,
        @PathParam("playerName") final String playerName) {

        this.service.addPlayerToGame(gameId, playerName);

        LOGGER.info(String.format("Successfully added player %s to game %d", playerName, gameId));

        return Response.noContent().build();
    }

    @DELETE
    @Path("/{gameId}/players/{playerName}")
    public Response deletePlayerFromGame(
        @PathParam("gameId") final Integer gameId,
        @PathParam("playerName") final String playerName) {

        this.service.removePlayerFromGame(gameId, playerName);

        LOGGER.info(String.format("Successfully removed player %s from game %d", playerName, gameId));

        return Response.noContent().build();
    }

    @POST
    @Path("/{gameId}/players/{playerName}/cards/{cardsCount}")
    public Response dealCardsToPlayer(
        @PathParam("gameId") final Integer gameId,
        @PathParam("playerName") final String playerName,
        @PathParam("cardsCount") final Integer cardsCount) {

        this.service.dealCardsToPlayer(gameId, playerName, cardsCount);

        LOGGER
            .info(String.format("Successfully dealt %d cards to player %s in game %d", cardsCount, playerName, gameId));

        return Response.ok().build();
    }

    @GET
    @Path("/{gameId}/players/{playerName}/cards")
    public Response getPlayerCards(
        @PathParam("gameId") final Integer gameId,
        @PathParam("playerName") final String playerName) {

        final List<Card> cards = this.service.getPlayerCards(gameId, playerName);
        final CardsResponse response = CardsResponse.fromCards(cards);

        LOGGER.info(String.format("Successfully retrieved cards of player %s in game %d", playerName, gameId));

        return Response.ok().entity(response).build();
    }

    @GET
    @Path("/{gameId}/players")
    public Response getPlayersCardsSummary(
        @PathParam("gameId") final Integer gameId) {

        final List<Player> players = this.service.getSortedPlayers(gameId);
        final PlayerSummaryResponse response = PlayerSummaryResponse.fromPlayers(players);

        LOGGER.info(String.format("Successfully retrieved players in game %d", gameId));

        return Response.ok().entity(response).build();
    }

    @GET
    @Path("/{gameId}/cards/summary")
    public Response getGameCardsSummary(
        @PathParam("gameId") final Integer gameId) {

        final Map<CardSuit, Long> cards = this.service.countRemainingCardsPerSuit(gameId);
        final CardsSummaryResponse response = CardsSummaryResponse.fromCards(cards);

        LOGGER.info(String.format("Successfully retrieved undealt cards per suit in game %d", gameId));

        return Response.ok().entity(response).build();
    }

    @GET
    @Path("/{gameId}/cards/count")
    public Response getGameCardsCount(
        @PathParam("gameId") final Integer gameId) {

        final Map<Card, Long> cards = this.service.countRemainingCardsPerCard(gameId);
        final CardsCountResponse response = CardsCountResponse.fromCards(cards);

        LOGGER.info(String.format("Successfully retrieved undealt cards count per card in game %d", gameId));

        return Response.ok().entity(response).build();
    }

    @POST
    @Path("/{gameId}/decks/shuffles")
    public Response shuffleCards(
        @PathParam("gameId") final Integer gameId) {

        this.service.shuffleCards(gameId);

        LOGGER.info(String.format("Successfully shuffled cards in game %d", gameId));

        return Response.noContent().build();
    }
}
