package com.example.service.exceptions;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;

@Provider
public final class JacksonExceptionMapper
    implements ExceptionMapper<JsonProcessingException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JacksonExceptionMapper.class);

    public JacksonExceptionMapper() {

        super();
    }

    @Override
    public Response toResponse(
        final JsonProcessingException exception) {

        LOGGER.debug(exception.getMessage(), exception);

        final JsonErrorResponse response =
            new JsonErrorResponse("400", "Bad request exception: " + exception.getClass().getSimpleName());

        return Response.status(BAD_REQUEST).type(MediaType.APPLICATION_JSON_TYPE).entity(response).build();
    }
}
