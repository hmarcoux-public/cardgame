package com.example.service.exceptions;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class JsonErrorResponse
    implements Serializable {

    private static final long serialVersionUID = 728245396808418018L;

    private final String errorCode;

    private final String message;

    @JsonCreator
    public JsonErrorResponse(
        @JsonProperty("code") final String errorCode,
        @JsonProperty("message") final String message) {

        this.errorCode = errorCode;
        this.message = message;
    }

    @JsonProperty("code")
    public String getErrorCode() {

        return this.errorCode;
    }

    @JsonProperty("message")
    public String getMessage() {

        return this.message;
    }

}
