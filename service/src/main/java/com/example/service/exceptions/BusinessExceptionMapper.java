package com.example.service.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.domain.exceptions.BusinessException;

@Provider
public final class BusinessExceptionMapper
    implements ExceptionMapper<BusinessException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(BusinessExceptionMapper.class);

    public BusinessExceptionMapper() {

        super();
    }

    @Override
    public Response toResponse(
        final BusinessException exception) {

        LOGGER.debug(exception.getMessage(), exception);

        final JsonErrorResponse response =
            new JsonErrorResponse(exception.getErrorCode().toString(), exception.getMessage());
        final Response.Status status = Response.Status.fromStatusCode(exception.getErrorCode());

        return Response.status(status).type(MediaType.APPLICATION_JSON_TYPE).entity(response).build();
    }
}
