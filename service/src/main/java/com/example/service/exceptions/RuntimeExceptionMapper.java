package com.example.service.exceptions;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public final class RuntimeExceptionMapper
    implements ExceptionMapper<RuntimeException> {

    private static final Logger LOGGER = LoggerFactory.getLogger(RuntimeExceptionMapper.class);

    public RuntimeExceptionMapper() {

        super();
    }

    @Override
    public Response toResponse(
        final RuntimeException exception) {

        LOGGER.error(exception.getMessage(), exception);

        final JsonErrorResponse response =
            new JsonErrorResponse("500", "Unexpected exception: " + exception.getClass().getSimpleName());

        return Response.status(INTERNAL_SERVER_ERROR).type(MediaType.APPLICATION_JSON_TYPE).entity(response).build();
    }
}
