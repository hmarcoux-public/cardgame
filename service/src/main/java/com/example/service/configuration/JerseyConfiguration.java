package com.example.service.configuration;

import javax.annotation.PostConstruct;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

import com.example.service.controllers.DeckController;
import com.example.service.controllers.GameController;
import com.example.service.exceptions.BusinessExceptionMapper;
import com.example.service.exceptions.JacksonExceptionMapper;
import com.example.service.exceptions.RuntimeExceptionMapper;
import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Configuration
public class JerseyConfiguration
    extends ResourceConfig {

    public JerseyConfiguration() {

        super();
    }

    @PostConstruct
    public void jerseyConfiguration() {

        register(GameController.class);
        register(DeckController.class);

        register(BusinessExceptionMapper.class);
        register(JacksonExceptionMapper.class);
        register(RuntimeExceptionMapper.class);

        register(JacksonJsonProvider.class);
    }
}
