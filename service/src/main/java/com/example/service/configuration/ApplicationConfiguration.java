package com.example.service.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application")
@ServletComponentScan(basePackages = { "com.example.configuration" })
public class ApplicationConfiguration {

    ApplicationConfiguration() {

    }

}
