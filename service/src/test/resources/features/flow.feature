Feature: Testing end to end flows from a consumer of the API perspective

  Background:
    Given No existing games and decks

  Scenario: Basic flow with single player
    Given I create a new game
    And I create a new deck
    And I add deck 1 to game 1
    And I add player 'John' to game 1
    And I shuffle cards in game 1
    When I deal 5 cards to 'John' in game 1
    Then cards can be retrieved from player 'John' in game 1

  Scenario: Complex flow with multiple decks, multiple players and all services invoked
    Given I create a new game
    And I create a new deck
    And I create a new deck
    And I add deck 1 to game 1
    And I add deck 2 to game 1
    And I add player 'John' to game 1
    And I add player 'Steeve' to game 1
    And I add player 'Dan' to game 1
    And I shuffle cards in game 1
    When I deal 5 cards to 'John' in game 1
    And I deal 5 cards to 'Steeve' in game 1
    And I deal 5 cards to 'Dan' in game 1
    Then cards can be retrieved from player 'John' in game 1
    And cards can be retrieved from player 'Steeve' in game 1
    And cards can be retrieved from player 'Dan' in game 1
    And players can be retrieved from game 1
    And undealt cards can be retrieved from game 1
    And undealt cards counts can be retrieved from game 1
