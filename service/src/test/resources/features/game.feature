Feature: Testing behaviours related to a game

  Background:
    Given No existing games and decks

  Scenario: Creating a new game successfully creates it
    When I create a new game
    Then response status is 201
    And game 1 exists

  Scenario: Deleting an existing game successfully deletes it
    Given I create a new game
    When I delete game with id 1
    Then response status is 204
    And game 1 doesn't exist

  Scenario: Deleting an unexisting game returns Not Found
    When I delete game with id 100
    Then response status is 404

  Scenario: Creating a new deck successfully creates it
    When I create a new deck
    Then response status is 201
    And deck 1 exists

  Scenario: Adding a deck to a game successfully adds it
    Given I create a new game
    And I create a new deck
    When I add deck 1 to game 1
    Then response status is 204
    And game 1 has deck 1

  Scenario: Adding a deck to an unexisting game returns Not Found
    Given I create a new deck
    When I add deck 1 to game 1
    Then response status is 404

  Scenario: Adding a player to a game successfully adds it
    Given I create a new game
    When I add player 'John' to game 1
    Then response status is 204

  Scenario: Adding a player to an unexisting game returns Not Found
    When I add player 'John' to game 1
    Then response status is 404

  Scenario: Deleting a player from a game successfully deletes it
    Given I create a new game
    And I add player 'John' to game 1
    When I delete player 'John' from game 1
    Then response status is 204

  Scenario: Deleting a player from an un existing game returns Not Found
    When I delete player 'John' from game 1
    Then response status is 404

  Scenario: Deleting an unexisting player from a game returns Not Found
    Given I create a new game
    When I delete player 'John' from game 1
    Then response status is 404

  Scenario: Shuffling cards in a game successfully shuffles them
    Given I create a new game
    And I create a new deck
    When I shuffle cards in game 1
    Then response status is 204

  Scenario: Shuffling cards in an un existing game returns Not Found
    When I shuffle cards in game 1
    Then response status is 404