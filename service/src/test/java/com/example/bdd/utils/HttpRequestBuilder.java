package com.example.bdd.utils;

import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

public final class HttpRequestBuilder {

    private static final String DEFAULT_BASE_CONTEXT_PATH = "";

    private static final String DEFAULT_BASE_URL = "http://localhost:%d";

    private static final int DEFAULT_PORT = 8080;

    private final HttpHeaders headers;

    private final String payload;

    private final TestRestTemplate client;

    private String baseUrl;

    private String baseContextPath;

    private HttpRequestBuilder(
        final TestRestTemplate client) {

        this.client = client;
        this.baseContextPath = DEFAULT_BASE_CONTEXT_PATH;
        this.baseUrl = String.format(DEFAULT_BASE_URL, DEFAULT_PORT);
        this.headers = new HttpHeaders();
        this.payload = null;
    }

    public static HttpRequestBuilder newJsonRequest(
        final TestRestTemplate client) {

        return new HttpRequestBuilder(client).withContentTypeHeader(MediaType.APPLICATION_JSON_UTF8_VALUE);
    }

    private HttpRequestBuilder withContentTypeHeader(
        final String value) {

        this.headers.add("Content-Type", value);
        return this;
    }

    public HttpRequestBuilder withPort(
        final int overriddenPort) {

        this.baseUrl = String.format(DEFAULT_BASE_URL, overriddenPort);
        return this;
    }

    public HttpRequestBuilder withBaseContextPath(
        final String overriddenBaseContextPath) {

        this.baseContextPath = overriddenBaseContextPath;
        return this;
    }

    public ResponseEntity<String> postTo(
        final String endpointUrl,
        final Object... urlParameters) {

        return sendToFullUrl(resolveUrl(endpointUrl, urlParameters), HttpMethod.POST);
    }

    public ResponseEntity<String> putTo(
        final String endpointUrl,
        final Object... urlParameters) {

        return sendToFullUrl(resolveUrl(endpointUrl, urlParameters), HttpMethod.PUT);
    }

    public ResponseEntity<String> deleteFrom(
        final String endpointUrl,
        final Object... urlParameters) {

        return sendToFullUrl(resolveUrl(endpointUrl, urlParameters), HttpMethod.DELETE);
    }

    public ResponseEntity<String> getFrom(
        final String endpointUrl,
        final Object... urlParameters) {

        final String resolvedUrl = resolveUrl(endpointUrl, urlParameters);
        final HttpEntity<String> request = new HttpEntity<>(this.headers);

        return this.client.exchange(resolvedUrl, HttpMethod.GET, request, String.class);
    }

    private ResponseEntity<String> sendToFullUrl(
        final String endpointUrl,
        final HttpMethod requestMethod) {

        final String resolvedUrl = validateUrl(endpointUrl);
        final HttpEntity<String> request = new HttpEntity<>(this.payload, this.headers);

        return this.client.exchange(resolvedUrl, requestMethod, request, String.class);
    }

    private String resolveUrl(
        final String endpointUrl,
        final Object... urlParameters) {

        final String resolvedBaseUrl = this.baseUrl + this.baseContextPath + endpointUrl;

        return String.format(resolvedBaseUrl, urlParameters);
    }

    private String validateUrl(
        final String endpointUrl) {

        try {
            return new URL(endpointUrl).toString();
        } catch (final MalformedURLException exception) {
            throw new RuntimeException(exception);
        }
    }

}
