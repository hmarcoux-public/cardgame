package com.example.bdd.utils;

import org.apache.http.impl.NoConnectionReuseStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

public final class HttpClient {

    private HttpClient() {

    }

    public static TestRestTemplate create() {

        final TestRestTemplate client = new TestRestTemplate();
        client.getRestTemplate().setRequestFactory(requestFactory());

        return client;
    }

    private static ClientHttpRequestFactory requestFactory() {

        final PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setMaxTotal(3);
        connectionManager.setDefaultMaxPerRoute(1);

        final HttpClientBuilder httpClientBuilder = HttpClientBuilder.create();
        httpClientBuilder.setConnectionManager(connectionManager);
        httpClientBuilder.setConnectionReuseStrategy(NoConnectionReuseStrategy.INSTANCE);

        final HttpComponentsClientHttpRequestFactory factory =
            new HttpComponentsClientHttpRequestFactory(httpClientBuilder.build());
        factory.setReadTimeout(45000);
        factory.setConnectTimeout(15000);

        return factory;
    }
}
