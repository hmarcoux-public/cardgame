package com.example.bdd.steps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.example.bdd.BDDConfiguration;
import com.example.service.Application;

import cucumber.api.java.Before;

@ContextConfiguration(classes = BDDConfiguration.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public final class BackgroundSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackgroundSteps.class);

    public BackgroundSteps() {

        super();
    }

    @Before
    public void initialize() {

        LOGGER.info("-------------- Initializing Spring context for Cucumber tests --------------");
    }
}
