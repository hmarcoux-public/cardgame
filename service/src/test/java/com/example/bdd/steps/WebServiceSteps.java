package com.example.bdd.steps;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

import java.io.IOException;
import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;

import com.example.bdd.utils.HttpClient;
import com.example.bdd.utils.HttpRequestBuilder;
import com.example.domain.Deck;
import com.example.domain.DeckRepository;
import com.example.domain.Game;
import com.example.domain.GameRepository;
import com.example.domain.exceptions.GameNotFoundException;
import com.example.service.controllers.messaging.CreateGameResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@Scope("cucumber-glue")
public final class WebServiceSteps {

    private static final TestRestTemplate HTTP_CLIENT = HttpClient.create();

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceSteps.class);

    @Value("${server.port:8080}")
    private int port;

    @Value("${server.servlet.context-path}")
    private String contextPath;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private GameRepository gameRepository;

    @Autowired
    private DeckRepository deckRepository;

    private ResponseEntity<String> response;

    protected WebServiceSteps() {

        super();
    }

    @Given("No existing games and decks")
    public void noExistingGamesAndDecks() {

        LOGGER.info("Clearing all games and decks");

        this.gameRepository.clearAll();
        this.deckRepository.clearAll();
    }

    @Then("response status is {int}")
    public void responseStatusIs(
        final Integer expectedStatus) {

        assertThat(this.response.getStatusCodeValue()).isEqualTo(expectedStatus);
    }

    @And("game has id {int}")
    public void createdGameHasId(
        final Integer expectedGameId) {

        assertThat(this.response.hasBody()).isTrue();
        final CreateGameResponse response = deserialize(this.response.getBody(), CreateGameResponse.class);

        assertThat(response.getGameId()).isEqualTo(expectedGameId);
    }

    @Given("I create a new game")
    public void createNewGame() {

        this.response = newRequest().postTo("/games");
        assertThat(this.response).isNotNull();
    }

    @When("I delete game with id {int}")
    public void deleteGameWithId(
        final Integer gameId) {

        this.response = newRequest().deleteFrom("/games/%d", gameId);
        assertThat(this.response).isNotNull();
    }

    @And("game {int} doesn't exist")
    public void gameDoesNotExist(
        final Integer gameId) {

        final Throwable throwable = catchThrowable(() -> this.gameRepository.fetchById(gameId));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @And("game {int} exists")
    public void gameExists(
        final Integer gameId) {

        final Game game = this.gameRepository.fetchById(gameId);

        assertThat(game).isNotNull();
    }

    @When("I create a new deck")
    public void createNewDeck() {

        this.response = newRequest().postTo("/decks");
        assertThat(this.response).isNotNull();
    }

    @And("deck {int} exists")
    public void deckExists(
        final Integer deckId) {

        final Deck deck = this.deckRepository.fetchById(deckId);

        assertThat(deck).isNotNull();
    }

    @When("I add deck {int} to game {int}")
    public void addDeckToGame(
        final Integer deckId,
        final Integer gameId) {

        this.response = newRequest().putTo("/games/%d/decks/%d", gameId, deckId);
        assertThat(this.response).isNotNull();
    }

    @And("game {int} has deck {int}")
    public void gameHasDeck(
        final Integer gameId,
        final Integer deckId) {

        final Game game = this.gameRepository.fetchById(gameId);

        assertThat(game.containsDeck(deckId)).isTrue();
    }

    private HttpRequestBuilder newRequest() {

        return HttpRequestBuilder.newJsonRequest(HTTP_CLIENT).withPort(this.port).withBaseContextPath(this.contextPath);
    }

    private <T extends Serializable> T deserialize(
        final String jsonString,
        final Class<T> classToDeserializeInto) {

        try {
            return this.objectMapper.readValue(jsonString, classToDeserializeInto);
        } catch (final IOException exception) {
            throw new RuntimeException(
                String.format("Cannot deserialize json string '' into class ''", jsonString, classToDeserializeInto),
                exception);
        }
    }

    @When("I add player {string} to game {int}")
    public void addPlayerToGame(
        final String playerName,
        final Integer gameId) {

        this.response = newRequest().putTo("/games/%d/players/%s", gameId, playerName);
        assertThat(this.response).isNotNull();
    }

    @When("I delete player {string} from game {int}")
    public void deletePlayerFromGame(
        final String playerName,
        final Integer gameId) {

        this.response = newRequest().deleteFrom("/games/%d/players/%s", gameId, playerName);
        assertThat(this.response).isNotNull();
    }

    @And("I shuffle cards in game {int}")
    public void shuffleCardsInGame(
        final Integer gameId) {

        this.response = newRequest().postTo("/games/%d/decks/shuffles", gameId);
        assertThat(this.response).isNotNull();
    }

    @When("I deal {int} cards to {string} in game {int}")
    public void dealCardsToPlayerInGame(
        final Integer cardsCount,
        final String playerName,
        final Integer gameId) {

        this.response = newRequest().postTo("/games/%d/players/%s/cards/%d", gameId, playerName, cardsCount);
        assertThat(this.response).isNotNull();
        responseStatusIs(200);
    }

    @Then("cards can be retrieved from player {string} in game {int}")
    public void cardsCanBeRetrievedFromPlayerInGame(
        final String playerName,
        final Integer gameId) {

        this.response = newRequest().getFrom("/games/%d/players/%s/cards", gameId, playerName);
        assertThat(this.response).isNotNull();
        responseStatusIs(200);

        LOGGER.info("RESPONSE: " + this.response.getBody());
    }

    @And("players can be retrieved from game {int}")
    public void playersCanBeRetrievedFromGame(
        final Integer gameId) {

        this.response = newRequest().getFrom("/games/%d/players", gameId);
        assertThat(this.response).isNotNull();
        responseStatusIs(200);

        LOGGER.info("RESPONSE: " + this.response.getBody());
    }

    @And("undealt cards can be retrieved from game {int}")
    public void undealtCardsCanBeRetrievedFromGame(
        final Integer gameId) {

        this.response = newRequest().getFrom("/games/%d/cards/summary", gameId);
        assertThat(this.response).isNotNull();
        responseStatusIs(200);

        LOGGER.info("RESPONSE: " + this.response.getBody());
    }

    @And("undealt cards counts can be retrieved from game {int}")
    public void undealtCardsCountsCanBeRetrievedFromGame(
        final Integer gameId) {

        this.response = newRequest().getFrom("/games/%d/cards/count", gameId);
        assertThat(this.response).isNotNull();
        responseStatusIs(200);

        LOGGER.info("RESPONSE: " + this.response.getBody());
    }
}
