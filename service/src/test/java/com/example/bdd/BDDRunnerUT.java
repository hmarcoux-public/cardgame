package com.example.bdd;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
    strict = true,
    features = "src/test/resources/features",
    glue = { "com.example.bdd" },
    plugin = { "pretty", "html:target/cucumber/example", "json:target/cucumber/example.json" })
public final class BDDRunnerUT {

    private BDDRunnerUT() {

        super();
    }
}
