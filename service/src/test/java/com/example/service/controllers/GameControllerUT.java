package com.example.service.controllers;

import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.domain.Game;
import com.example.domain.GameDomainService;
import com.example.domain.GameRepository;
import com.example.domain.exceptions.DeckNotFoundException;
import com.example.domain.exceptions.GameNotFoundException;
import com.example.domain.exceptions.PlayerNotFoundException;
import com.example.service.controllers.messaging.CreateGameResponse;

@RunWith(MockitoJUnitRunner.class)
public final class GameControllerUT {

    private static final int GAME_ID = 1;

    private static final int DECK_ID = 5;

    private static final int UNEXISTING_GAME_ID = 100;

    private static final int UNEXISTING_DECK_ID = 99;

    private static final String PLAYER_NAME = "AnyPlayer";

    private static final GameNotFoundException GAME_NOT_FOUND_EXCEPTION = new GameNotFoundException(UNEXISTING_GAME_ID);

    private static final DeckNotFoundException DECK_NOT_FOUND_EXCEPTION = new DeckNotFoundException(UNEXISTING_DECK_ID);

    private static final PlayerNotFoundException PLAYER_NOT_FOUND_EXCEPTION = new PlayerNotFoundException(PLAYER_NAME);

    private static final Integer CARDS_TO_DEAL_COUNT = 5;

    @Mock
    private GameRepository repository;

    @Mock
    private Game game;

    @Mock
    private GameDomainService service;

    @InjectMocks
    private GameController controller;

    public GameControllerUT() {

        super();
    }

    @Test
    public void whenCreatingNewGame_thenGameIsSuccessfullyCreated() {

        given(this.game.getId()).willReturn(GAME_ID);
        given(this.repository.createGame()).willReturn(this.game);

        final Response response = this.controller.createGame();

        assertThat(response.getStatusInfo()).isEqualTo(CREATED);
    }

    @Test
    public void whenCreatingNewGame_thenGameIsCreatedWithExpectedId() {

        given(this.game.getId()).willReturn(GAME_ID);
        given(this.repository.createGame()).willReturn(this.game);

        final Response response = this.controller.createGame();
        final CreateGameResponse gameResponse = (CreateGameResponse) response.getEntity();

        assertThat(gameResponse.getGameId()).isEqualTo(GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenDeletingIt_thenGameIsDeleted() {

        this.controller.deleteGame(GAME_ID);

        verify(this.repository).delete(GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenDeletingIt_thenResponseStatusIsNoContent() {

        final Response response = this.controller.deleteGame(GAME_ID);

        assertThat(response.getStatusInfo()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void givenUnexistingGame_whenDeletingIt_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.repository).delete(UNEXISTING_GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.deleteGame(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenAddingDeckToIt_thenResponseStatusIsNoContent() {

        final Response response = this.controller.addDeckToGame(GAME_ID, DECK_ID);

        assertThat(response.getStatusInfo()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void givenAnExistingGame_whenAddingDeckToIt_thenDeckIsAdded() {

        this.controller.addDeckToGame(GAME_ID, DECK_ID);

        verify(this.service).addDeckToGame(GAME_ID, DECK_ID);
    }

    @Test
    public void givenUnexistingGame_whenAddingDeckToIt_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).addDeckToGame(UNEXISTING_GAME_ID, DECK_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.addDeckToGame(UNEXISTING_GAME_ID, DECK_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingDeck_whenAddingDeckToGame_thenDeckNotFoundExceptionIsThrown() {

        doThrow(DECK_NOT_FOUND_EXCEPTION).when(this.service).addDeckToGame(UNEXISTING_GAME_ID, DECK_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.addDeckToGame(UNEXISTING_GAME_ID, DECK_ID));

        assertThat(throwable).isInstanceOf(DeckNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenAddingPlayerToIt_thenPlayerIsAdded() {

        this.controller.addPlayerToGame(GAME_ID, PLAYER_NAME);

        verify(this.service).addPlayerToGame(GAME_ID, PLAYER_NAME);
    }

    @Test
    public void givenAnExistingGame_whenAddingPlayerToIt_thenResponseStatusIsNoContent() {

        final Response response = this.controller.addPlayerToGame(GAME_ID, PLAYER_NAME);

        assertThat(response.getStatusInfo()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void givenAnExistingGame_whenRemovingPlayerFromIt_thenPlayerIsRemoved() {

        this.controller.deletePlayerFromGame(GAME_ID, PLAYER_NAME);

        verify(this.service).removePlayerFromGame(GAME_ID, PLAYER_NAME);
    }

    @Test
    public void givenAnExistingGame_whenRemovingPlayerFromIt_thenResponseStatusIsNoContent() {

        final Response response = this.controller.deletePlayerFromGame(GAME_ID, PLAYER_NAME);

        assertThat(response.getStatusInfo()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void givenUnexistingGame_whenRemovingPlayerFromIt_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).removePlayerFromGame(UNEXISTING_GAME_ID, PLAYER_NAME);

        final Throwable throwable =
            catchThrowable(() -> this.controller.deletePlayerFromGame(UNEXISTING_GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingPlayer_whenRemovingPlayerFromGame_thenPlayerNotFoundExceptionIsThrown() {

        doThrow(PLAYER_NOT_FOUND_EXCEPTION).when(this.service).removePlayerFromGame(UNEXISTING_GAME_ID, PLAYER_NAME);

        final Throwable throwable =
            catchThrowable(() -> this.controller.deletePlayerFromGame(UNEXISTING_GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(PlayerNotFoundException.class);
    }

    @Test
    public void givenAnExistingGameAndExistingPlayer_whenDealingCardsToPlayer_thenPlayerHasDealtCards() {

        this.controller.dealCardsToPlayer(GAME_ID, PLAYER_NAME, CARDS_TO_DEAL_COUNT);

        verify(this.service).dealCardsToPlayer(GAME_ID, PLAYER_NAME, CARDS_TO_DEAL_COUNT);
    }

    @Test
    public void givenAnExistingGameAndExistingPlayer_whenDealingCardsToPlayer_thenResponseStatusIsOk() {

        final Response response = this.controller.dealCardsToPlayer(GAME_ID, PLAYER_NAME, CARDS_TO_DEAL_COUNT);

        assertThat(response.getStatusInfo()).isEqualTo(OK);
    }

    @Test
    public void givenUnexistingGame_whenDealingCardsToPlayer_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).dealCardsToPlayer(UNEXISTING_GAME_ID, PLAYER_NAME,
            CARDS_TO_DEAL_COUNT);

        final Throwable throwable =
            catchThrowable(
                () -> this.controller.dealCardsToPlayer(UNEXISTING_GAME_ID, PLAYER_NAME, CARDS_TO_DEAL_COUNT));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingPlayer_whenDealingCardsToPlayer_thenPlayerNotFoundExceptionIsThrown() {

        doThrow(PLAYER_NOT_FOUND_EXCEPTION).when(this.service).dealCardsToPlayer(UNEXISTING_GAME_ID, PLAYER_NAME,
            CARDS_TO_DEAL_COUNT);

        final Throwable throwable =
            catchThrowable(
                () -> this.controller.dealCardsToPlayer(UNEXISTING_GAME_ID, PLAYER_NAME, CARDS_TO_DEAL_COUNT));

        assertThat(throwable).isInstanceOf(PlayerNotFoundException.class);
    }

    @Test
    public void givenAnExistingGameAndExistingPlayer_whenGettingPlayersCards_thenPlayerCardsAreReturned() {

        this.controller.getPlayerCards(GAME_ID, PLAYER_NAME);

        verify(this.service).getPlayerCards(GAME_ID, PLAYER_NAME);
    }

    @Test
    public void givenAnExistingGameAndExistingPlayer_whenGettingPlayersCards_thenResponseStatusIsOk() {

        final Response response = this.controller.getPlayerCards(GAME_ID, PLAYER_NAME);

        assertThat(response.getStatusInfo()).isEqualTo(OK);
    }

    @Test
    public void givenUnexistingGame_whenGettingPlayersCards_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).getPlayerCards(UNEXISTING_GAME_ID, PLAYER_NAME);

        final Throwable throwable =
            catchThrowable(() -> this.controller.getPlayerCards(UNEXISTING_GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenExistingGameButUnexistingPlayer_whenGettingPlayersCards_thenPlayerNotFoundExceptionIsThrown() {

        doThrow(PLAYER_NOT_FOUND_EXCEPTION).when(this.service).getPlayerCards(UNEXISTING_GAME_ID, PLAYER_NAME);

        final Throwable throwable =
            catchThrowable(() -> this.controller.getPlayerCards(UNEXISTING_GAME_ID, PLAYER_NAME));

        assertThat(throwable).isInstanceOf(PlayerNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenGettingPlayersCardSummary_thenSummariesAreReturned() {

        this.controller.getPlayersCardsSummary(GAME_ID);

        verify(this.service).getSortedPlayers(GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenGettingPlayersCardSummary_thenResponseStatusIsOk() {

        final Response response = this.controller.getPlayersCardsSummary(GAME_ID);

        assertThat(response.getStatusInfo()).isEqualTo(OK);
    }

    @Test
    public void givenUnexistingGame_whenGettingPlayersCardSummary_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).getSortedPlayers(UNEXISTING_GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.getPlayersCardsSummary(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenGettingGameCardsSummary_thenSummariesAreReturned() {

        this.controller.getGameCardsSummary(GAME_ID);

        verify(this.service).countRemainingCardsPerSuit(GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenGettingGameCardsSummary_thenResponseStatusIsOk() {

        final Response response = this.controller.getGameCardsSummary(GAME_ID);

        assertThat(response.getStatusInfo()).isEqualTo(OK);
    }

    @Test
    public void givenUnexistingGame_whenGettingGameCardsSummary_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).countRemainingCardsPerSuit(UNEXISTING_GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.getGameCardsSummary(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenGettingGameCardsCount_thenCountsAreReturned() {

        this.controller.getGameCardsCount(GAME_ID);

        verify(this.service).countRemainingCardsPerCard(GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenGettingGameCardsCount_thenResponseStatusIsOk() {

        final Response response = this.controller.getGameCardsCount(GAME_ID);

        assertThat(response.getStatusInfo()).isEqualTo(OK);
    }

    @Test
    public void givenUnexistingGame_whenGettingGameCardsCount_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).countRemainingCardsPerCard(UNEXISTING_GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.getGameCardsCount(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }

    @Test
    public void givenAnExistingGame_whenShufflingCards_thenCountsAreReturned() {

        this.controller.shuffleCards(GAME_ID);

        verify(this.service).shuffleCards(GAME_ID);
    }

    @Test
    public void givenAnExistingGame_whenShufflingCards_thenResponseStatusIsNoContent() {

        final Response response = this.controller.shuffleCards(GAME_ID);

        assertThat(response.getStatusInfo()).isEqualTo(NO_CONTENT);
    }

    @Test
    public void givenUnexistingGame_whenShufflingCards_thenGameNotFoundExceptionIsThrown() {

        doThrow(GAME_NOT_FOUND_EXCEPTION).when(this.service).shuffleCards(UNEXISTING_GAME_ID);

        final Throwable throwable = catchThrowable(() -> this.controller.shuffleCards(UNEXISTING_GAME_ID));

        assertThat(throwable).isInstanceOf(GameNotFoundException.class);
    }
}
