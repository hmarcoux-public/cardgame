package com.example.service.controllers;

import static javax.ws.rs.core.Response.Status.CREATED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import javax.ws.rs.core.Response;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.example.domain.Deck;
import com.example.domain.DeckRepository;
import com.example.service.controllers.messaging.CreateDeckResponse;

@RunWith(MockitoJUnitRunner.class)
public final class DeckControllerUT {

    private static final int DECK_ID = 1;

    @Mock
    private DeckRepository repository;

    @Mock
    private Deck deck;

    @InjectMocks
    private DeckController controller;

    public DeckControllerUT() {

        super();
    }

    @Test
    public void whenCreatingNewDeck_thenDeckIsSuccessfullyCreated() {

        given(this.deck.getId()).willReturn(DECK_ID);
        given(this.repository.createDeck()).willReturn(this.deck);

        final Response response = this.controller.createDeck();

        assertThat(response.getStatusInfo()).isEqualTo(CREATED);
    }

    @Test
    public void whenCreatingNewDeck_thenDeckIsCreatedWithExpectedId() {

        given(this.deck.getId()).willReturn(DECK_ID);
        given(this.repository.createDeck()).willReturn(this.deck);

        final Response response = this.controller.createDeck();
        final CreateDeckResponse deckResponse = CreateDeckResponse.class.cast(response.getEntity());

        assertThat(deckResponse.getDeckId()).isEqualTo(DECK_ID);
    }
}
