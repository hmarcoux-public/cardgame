Example Project
===========

Running locally
---------------

To build the project from project's root directory:

```bash
mvn --update-snapshots --settings settings.xml clean package
```
Note that tests ran during the build will require ports 8080 and 8081 to run an embedded server.
Make sure those ports are not in use prior to building the project.  

To run the project from project's root directory, upon successful build:

```bash
java -jar service/target/com.example.example-service-1.0.0.jar
```

Tested with
---------------
- Maven: 3.6.1
- Java: OpenJDK 11.0.8